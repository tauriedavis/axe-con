# Axe Con

1. [Difference Drives Innovation & Disability Inclusion Benefits All of Us](https://gitlab.com/tauriedavis/axe-con/-/snippets/2088923) - Haben Girma
1. [ROI of Accessibility](https://gitlab.com/tauriedavis/axe-con/-/snippets/2088960) - Greg Williams
1. [Applied Accessibility: Practical Tips for Building More Accessible Front-Ends](https://gitlab.com/tauriedavis/axe-con/-/snippets/2088985) - Sara Soueidan
1. [Auditing Design Systems for Accessibility](https://gitlab.com/tauriedavis/axe-con/-/snippets/2089002) - Anna Cook
